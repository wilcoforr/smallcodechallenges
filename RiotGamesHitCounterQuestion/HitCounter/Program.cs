﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HitCounter
{
//    This question was asked by Riot Games.
//Design and implement a HitCounter class that keeps track of requests(or hits). It should support the following operations:
//•	record(timestamp) : records a hit that happened at timestamp
//•	total() : returns the total number of hits recorded
//•	range(lower, upper) : returns the number of hits that occurred between timestamps lower and upper(inclusive)
//Follow-up: What if our system has limited memory?
//november 21 2018
//3 minutes... i guess? and this is the first code I wrote all this morning. not doing followup in code.
// for follow up - an idea would be if the system has limited RAM memory - use on disk storage (SQL database)
// if limited disk storage on server - user client's computer for some storage, then have a "parity" system so the user
// just cant hack/cheat on the client

    public class HitCounter
    {
        public List<DateTime> hits = new List<DateTime>();

        public void Record(DateTime dateTime)
        {
            hits.Add(dateTime);
        }

        public int Total()
        {
            return hits.Count;
        }

        public int Range(DateTime lower, DateTime higher)
        {
            return hits
                .Where(r =>
                            r > lower
                            &&
                            r < higher
                            )
                .Count();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {


        }
    }
}
