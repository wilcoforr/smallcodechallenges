﻿namespace SingletonPatternTwist
{
    using System;

    class Person
    {
        public string Name { get; set; }
    }

    class Program
    {

        //This problem was asked by Microsoft.
        //Implement the singleton pattern with a twist. First, instead of storing one instance, store 
        //two instances. And in every even call of getInstance(), return the first instance and in 
        //every odd call of getInstance(), return the second instance.

        //Done. started at 9:43. Done at 9:48. 5min. Probably not 100% correct because this question is/was kinda whack.
        static void Main(string[] args)
        {
            One = new Person { Name = "One" };
            Two = new Person { Name = "Two" };

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(SingletonPersonTwist.Name);
            }

            Console.ReadLine();
        }

        static Person One { get; set; }

        static Person Two { get; set; }

        static int CallCount { get; set; }


        // "getInstance()"
        static Person SingletonPersonTwist
        {
            get
            {
                CallCount++;

                if (CallCount % 2 == 0)
                {
                    return One;
                }
                else
                {
                    return Two;
                }

            }
        }

    }
}
