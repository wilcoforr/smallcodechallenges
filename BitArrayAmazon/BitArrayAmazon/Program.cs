﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitArrayAmazon
{
//    This problem was asked by Amazon.
//Implement a bit array.
//A bit array is a space efficient array that holds a value of 1 or 0 at each index.
//•	init(size): initialize the array with size
//•	set(i, val): updates index at i with val where val is either 1 or 0.
//•	get(i): gets the value at index i.

    class BitArray
    {
        public bool[] array { get; set; }

        public static BitArray init(int size)
        {
            var bitArray = new BitArray();
            bitArray.array = new bool[size];

            return bitArray;
        }

        public void set(int i, int val)
        {
            array[i] = val == 1 ? true : false;
        }

        public bool get(int i)
        {
            return array[i];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
