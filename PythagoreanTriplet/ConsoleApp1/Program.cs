﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    //This problem was asked by Netflix.
    //Given an array of integers, determine whether it contains a Pythagorean triplet.
    //Recall that a Pythogorean triplet (a, b, c) is defined by the equation a2+ b2= c2.


    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pythagorean Triplets example!");

            Console.WriteLine($"Is Triplet? 3, 4, 5? {IsPythagoreanTriplet(new int[] { 3,4,5})}");
            Console.WriteLine();

            Console.WriteLine($"Is Triplet? 4, 5, 6? {IsPythagoreanTriplet(new int[] { 4, 5, 6 })}");
            Console.WriteLine();

            Console.WriteLine($"Is Triplet? 5, 12, 13? {IsPythagoreanTriplet(new int[] { 5, 12, 13 })}");
            Console.ReadLine();
        }


        static bool IsPythagoreanTriplet(int[] ints)
        {
            var tempInts = ints.OrderBy(i => i).ToArray();

            int aSquaredPlusBSquared = tempInts[0] * tempInts[0] + tempInts[1] * tempInts[1];

            int cSquared = tempInts[2] * tempInts[2];

            Console.WriteLine($"a^2 + b^2: {aSquaredPlusBSquared}");
            Console.WriteLine($"c^2: {cSquared}");

            return aSquaredPlusBSquared == cSquared;
        }

    }
}
