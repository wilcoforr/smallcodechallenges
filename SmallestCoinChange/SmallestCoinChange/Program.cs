﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallestCoinChange
{
//    This problem was asked by Google.
//Find the minimum number of coins required to make n cents.
//You can use standard American denominations, that is, 1¢, 5¢, 10¢, and 25¢.
//For example, given n = 16, return 3 since we can make it with a 10¢, a 5¢, and a 1¢.

    class Program
    {
        static void Main(string[] args)
        {


            Console.ReadLine();
        }

        static int ChangeCoins(int cents)
        {
            int coinCount = 0;

            int quarter = 25;
            int dime = 10;
            int nickel = 5;
            int penny = 1;

            if (cents == 25)
            {
                return 1;
            }
            else if (cents == 10)
            {
                return 1;
            }
            else if (cents == 5)
            {
                return 1;
            }
            else if (cents == 1)
            {
                return 1;
            }


            return coinCount;
        }
    }
}
