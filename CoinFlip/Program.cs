﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinFlip
{
    struct Coin
    {
        public bool IsHeads;
    }

    class Program
    {
        //You have 100 fair coins and you flip them all at the same time.
        //Any that come up tails you set aside.The ones that come up heads you flip again. 
        //How many rounds do you expect to play before only one coin remains?
        //Write a function that, given n, returns the number of rounds you'd expect to play until one coin remains.
        //FWW: Done. About 10min.
        static Random random = new Random();

        static void Main(string[] args)
        {
            var coins = new Coin[100];

            Console.WriteLine("Rounds till one coin: " + RoundsTillOneCoin(coins));

            Console.WriteLine("Enter to exit");
            Console.Read();
        }

        static int RoundsTillOneCoin(Coin[] coins) //n
        {
            int rounds = 1;

            while (coins.Any(c => c.IsHeads) || rounds == 1)
            {
                for (int i = 0; i < coins.Length; i++)
                {
                    coins[i].IsHeads = random.Next(2) == 0;
                }

                Console.WriteLine("Round count: " + rounds);
                Console.WriteLine("Heads: " + coins.Where(c => c.IsHeads).Count());
                Console.WriteLine("Tails: " + coins.Where(c => !c.IsHeads).Count());

                coins = coins.Where(c => c.IsHeads).ToArray();

                if (coins.Length == 1)
                {
                    return rounds;
                }

                rounds++;
            }

            return rounds;
        }
    }
}
