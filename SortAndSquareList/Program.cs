﻿namespace Nov_7_2018
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    // november 7 2018 question. Done in like 5-7 min (initially was int[] based, condensed into List using Linq for prettiness)
    //Given a sorted list of integers, square the elements and give the output in sorted order.
    //For example, given[-9, -2, 0, 2, 3], return [0, 4, 4, 9, 81].
    class Program
    {
        static Func<int, int> Square => i => i * i;

        static void Main(string[] args)
        {
            var input = new List<int> { -9, -2, 0, 2, 3 };

            Console.WriteLine("Input");

            Console.WriteLine(String.Join(", ", input));

            input = input.Select(Square)
                        .OrderBy(i => i)
                        .ToList();

            Console.WriteLine("\nSorted and squared");

            Console.WriteLine(String.Join(", ", input));

            Console.ReadLine();
        }
    }
}
