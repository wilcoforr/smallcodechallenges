using System;
using System.Collections.Generic;
using System.Linq;

namespace LongestSubarray
{
    class Program
    {
        //Given an array of elements, return the length of the longest subarray where all its elements are distinct.
        //For example, given the array[5, 1, 3, 5, 2, 3, 4, 1], return 5 as the longest subarray of distinct elements is [5, 2, 3, 4, 1].
        static void Main(string[] args)
        {
            var arr = new int[] { 5, 1, 3, 5, 2, 3, 4, 1 };

            Console.WriteLine(LongestSubarrayLength(arr));

            Console.WriteLine(LongestSubarrayLengthNoApi(arr));

            Console.ReadLine();
        }

        //done in like what 10 seconds? using .NET API though
        static int LongestSubarrayLength(int[] arr)
        {
            return arr.Distinct().Count();
        }


        //no .NET API for distinct... using a List though
        static int LongestSubarrayLengthNoApi(int[] arr)
        {
            var distinct = new List<int>();

            for (int i = 0; i < arr.Length; i++)
            {
                if (!distinct.Contains(arr[i]))
                {
                    distinct.Add(i);
                }
            }

            return distinct.Count;
        }

    }
}
